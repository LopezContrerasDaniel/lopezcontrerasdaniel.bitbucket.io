var config = {
    apiKey: "AIzaSyB2U-tcoXJBfByTsDlwrGncRGP0I0L9j3Y",
    authDomain: "maril-d27bf.firebaseapp.com",
    databaseURL: "https://maril-d27bf.firebaseio.com",
    projectId: "maril-d27bf",
    storageBucket: "maril-d27bf.appspot.com",
    messagingSenderId: "955449389554"
};
firebase.initializeApp(config);
// Initialize Cloud Firestore through Firebase
var db = firebase.firestore();
$(document).ready(function(){
    M.AutoInit();
    $('.carousel.carousel-slider').carousel({
        fullWidth: true,
      });
    $('.slider').slider({});
    $('.modal').modal({
      startingTop: '0%',
      endingTop: '0%',
      onOpenEnd: function() { $('.materialboxed').materialbox(); }
    });
  });
Vue.component('product-card', {
    // declara las propiedades
    props: ['img','title','price','about','id'],
    // como 'data', las propiedades pueden ser utilizadas dentro de las
    // plantillas y est� disponibles en la vm como this.message
    template: `
    <div :id="id" class="card modal-trigger" v-on:click="detalles" href="#modal1" >
        <div class="card-image">
        <img class="" :src="img[0]">
        </div>
        <div class="card-content">
        <span class="card-title black-text">{{ title }}</span>
        <h6 class="teal-text text-accent-3">$`+`{{ price }}</h6>
        <p class="truncate">{{ about }}</p>
        </div>
    </div>`,
    methods:{
        detalles: function(){
            app._data.img = this.img;
            app._data.titulo = this.title;
            app._data.precio = this.price;
            app._data.descripcion = this.about;
            
        }
    }
  })
  Vue.component('img-card', {
    props: ['img'],
    template: `
    <a title="slide" href="#"><img :src="img"  class="slideimg materialboxed" alt="foto" /></a>`,
  })
var app = new Vue({
    el: '#app',
    data: {
      productos:[],
      img:[],
      titulo:"",
      precio:"",
      descripcion:""
    }
  });

  db.collection("Productos").onSnapshot((querySnapshot) => {
    app._data.productos = [];
    querySnapshot.forEach((doc) => {
        app._data.productos.push({img: doc.data().imagen, title: doc.data().nombre, price: doc.data().precio, about: doc.data().descripcion, id:doc.id});
        $('.materialboxed').materialbox();
    });
});
