var Ajedrez = (function (){

//---------------------------------------------------------------------------------------------------------------------------------

    var _mostrar_tablero = function(){
        _actualizar_tablero();
    };

//---------------------------------------------------------------------------------------------------------------------------------

    var _mover_pieza = function( movimiento ){
        var actual =  document.getElementById(movimiento.de);
       var poner = document.getElementById(movimiento.a);
       var pieza = actual.innerHTML;
       
       if(actual.innerHTML != "" && poner.innerHTML == "∅"){
        poner.innerText = pieza;
        actual.innerHTML="∅";    
        
       }else{
        alert("movimiento invalido"); 
       }
    };

//----------------------------------------------------------------------------------------------------------------------------------


    var _actualizar_tablero = function(){
        var _tablero_ajedrez = document.getElementById("tablero");
        var _contenedor_boton = document.getElementById( "opcion" );
        var _boton_descarga = document.createElement("button");
        _boton_descarga.textContent = "Actualizar";
        _contenedor_boton.appendChild( _boton_descarga );
        _boton_descarga.addEventListener( "click" ,function() {_evento_click()} );

        var _evento_click = function(){
    
        var archivo_a_descargar = document.getElementById("archivo_a_descargar");
        var servidor = "https://lopezcontrerasdaniel.bitbucket.io";
        var url = servidor + "/csv/tablero.csv" + "?r=" + Math.random();
        var xhr = new XMLHttpRequest();

        var tabla = document.getElementsByTagName("table")[0];   
            if (!tabla){

            } else {
                padre = tabla.parentNode;
                padre.removeChild(tabla);
            } 

        var _tabla_ajedrez = document.createElement( "table" );
        _tablero_ajedrez.appendChild( _tabla_ajedrez );
        xhr.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status === 200) {

                    var contenido = xhr.responseText;
                    var i = 15;
                    var cordenadas = ['a','b','c','d','e','f','h','i'];
                    var x = 0;
                    var y = 9;
                    for( var cont = i; cont < contenido.length; cont++ )
                    {
                        var _fila;
                        if( contenido[cont] === "\n" || cont === i)
                        {
                            _fila = document.createElement( "tr" );
                            _tabla_ajedrez.appendChild( _fila );
                            y--;
                        }
                        if( contenido[cont] !== '|' && contenido[cont] !== "\n")
                        {
                            var _elemento = document.createElement( "td" );
                            _elemento.textContent = contenido[cont];
                            _elemento.setAttribute("id", cordenadas[x]+y);
                            _fila.appendChild( _elemento );
                            x++;
                            if(x>7){
                                x=0;
                            }
                        }
                        
                    }

                } else {
                alert("Error al conectar con el servidor\n" + "Status: " + this.status  + "\nUrl: " + this.responseURL);
                }
        }
        };
        xhr.open('GET', url, true);
        xhr.send();

    };
    };
    
//------------------------------------------------------------------------------------------------------------------------------                        

    return{
        'mostrarTablero': _mostrar_tablero,
        'actualizarTablero': _actualizar_tablero,
        'moverPieza': _mover_pieza
    }

})();